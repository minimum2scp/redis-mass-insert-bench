#! /usr/bin/env ruby

require 'bundler/setup'
require 'redis'
require 'hiredis'
require 'thor'

class Bench < Thor
  class_option :number_of_keys, :aliases => ['-n'], :type => :numeric, :default => 1_000_000
  class_option :redis_host,     :aliases => ['-h'], :type => :string,  :default => '127.0.0.1'
  class_option :redis_port,     :aliases => ['-p'], :type => :numeric, :default => 6379
  class_option :redis_db,       :aliases => ['-d'], :type => :numeric, :default => 1
  class_option :bench_loops,    :aliases => ["-l"], :type => :numeric, :default => 3

  desc "benchmark", "run benchmark"
  def benchmark
    require 'benchmark'
    Benchmark.bm(32,
                 '<avg> redis_cli',
                 '<avg> redis_cli_with_pipe',
                 '<avg> pipelined',
                 '<avg> piplined_with_hiredis') do |x|
      rep = {
        :redis_cli => [],
        :redis_cli_with_pipe => [],
        :pipelined => [],
        :pipelined_with_hiredis => [],
      }
      options[:bench_loops].times do |i|
        x.report("flush"){ flush }

        t = x.report("redis_cli"){
          system("#{$0} redis_cli -n #{options[:number_of_keys]} | " +
                 "redis-cli --pipe -h #{options[:redis_host]} -p #{options[:redis_port]} -n #{options[:redis_db]} 1>/dev/null") }
        rep[:redis_cli] << t

        x.report("flush"){ flush }

        t = x.report("redis_cli_with_pipe"){
          cmdline = File.read("/proc/#{$$}/cmdline").split("\x0").join(' ').sub(/\bbenchmark\b/, ' redis_cli_with_pipe ')
          system(cmdline)
        }

        rep[:redis_cli_with_pipe] << t

        x.report("flush"){ flush }
        t = x.report("pipelined"){
          cmdline = File.read("/proc/#{$$}/cmdline").split("\x0").join(' ').sub(/\bbenchmark\b/, ' pipelined ')
          system(cmdline)
        }
        rep[:pipelined] << t
        x.report("flush"){ flush }
        t = x.report("pipelined_with_hiredis"){
          cmdline = File.read("/proc/#{$$}/cmdline").split("\x0").join(' ').sub(/\bbenchmark\b/, ' pipelined_with_hiredis ')
          system(cmdline)
        }
        rep[:pipelined_with_hiredis] << t
      end
      rep.keys.map do |k|
        rep[k].inject(&:+) / options[:bench_loops]
      end
    end
  end

  desc "info", "info memory, keyspace"
  def info
    show_redis_info
  end

  desc "flush", "delete all data in redis"
  def flush
    redis = new_redis_connection
    redis.flushall
    redis.quit
  end

  desc "redis_cli", "output mass insert protocol for redis-cli --pipe"
  def redis_cli
    io = $stdout
    (1..options[:number_of_keys]).each do |i|
      args = ['SET', "foo#{i}", "bar#{i}"]
      io.write gen_redis_protocol(args)
    end
  end

  desc "redis_cli_with_pip", "pipe to redis-cli --pipe with mass insert protocol"
  def redis_cli_with_pipe
    io = new_redis_cli_io("--pipe")
    (1..options[:number_of_keys]).each do |i|
      args = ['SET', "foo#{i}", "bar#{i}"]
      io.write gen_redis_protocol(args)
    end
    io.close
  end

  desc "pipelined", "ruby redis gem with pipelined"
  def pipelined
    redis = new_redis_connection
    redis.pipelined do
      (1..options[:number_of_keys]).each do |i|
        redis.set("foo#{i}", "bar#{i}")
      end
    end
  end

  desc "pipelined_with_hiredis", "ruby redis gem with pipelined, with hiredis driver"
  def pipelined_with_hiredis
    redis = new_redis_connection(driver: :hiredis)
    redis.pipelined do
      (1..options[:number_of_keys]).each do |i|
        redis.set("foo#{i}", "bar#{i}")
      end
    end
  end

  no_commands do
    def new_redis_connection(driver: false)
      if driver
        Redis.new(host: options[:redis_host], port: options[:redis_port], db: options[:redis_db], driver: driver)
      else
        Redis.new(host: options[:redis_host], port: options[:redis_port], db: options[:redis_db])
      end
    end

    def new_redis_cli_io(other_opts)
      open("| redis-cli -h #{options[:redis_host]} -p #{options[:redis_port]} -n #{options[:redis_db]} #{other_opts} 1>/dev/null", "w")
    end

    ## see: "Redis Mass Insertion" "Generating Redis Protocol"
    ##      (http://redis.io/topics/mass-insert)
    def gen_redis_protocol(args)
      proto = ""
      proto << "*#{args.length.to_s}\r\n"
      args.each do |arg|
        proto << "$#{arg.to_s.bytesize.to_s}\r\n"
        proto << "#{arg.to_s}\r\n"
      end
      proto
    end

    def show_redis_info
      redis = new_redis_connection(driver: :hiredis)
      puts "info: memory"
      redis.info("memory").each do |k,v|
        puts "  #{k}: #{v}"
      end
      puts ""
      puts "info: keyspace"
      redis.info("keyspace").each do |k,v|
        puts "  #{k}: #{v}"
      end
      puts ""
    end
  end
end

Bench.start
